function build_tools (compiler)

if ispc
    exe_ext = '.exe';
else
    exe_ext = '';
end

if ~exist(fullfile('.', 'tools', ['MATLAB2C++' exe_ext]), 'file')
    if nargin < 1
        disp('The argument ''compiler'' of build_tools is empty.');
        disp('Find an available compiler.');
        compiler = tools.detect_compiler();
    end

    disp('Build MATLAB2C++.');
    [status, out] = compiler({fullfile('.', 'tools', 'matlab2c++.cpp')}, fullfile('.', 'tools', ['MATLAB2C++' exe_ext]));

    if status ~= 0
        disp(out);
        error('Build of MATLAB2C++ failed.');
    else
        disp('Build of MATLAB2C++ succeeded.');
    end

    delete('*.obj');
end

if ~exist(fullfile('.', 'tools', ['ReduceAffine', exe_ext]), 'file')
    if nargin < 1 && ~exist('compiler', 'var')
        disp('The argument ''compiler'' of build_tools is empty.');
        disp('Find an available compiler.');
        compiler = tools.detect_compiler();
    end

    disp('Build ReduceAffine.');
    [status, out] = compiler({fullfile('.', 'tools', 'reduce_affine.cpp')}, fullfile('.', 'tools', ['ReduceAffine' exe_ext]));

    if status ~= 0
        disp(out);
        error('Build of ReduceAffine failed.');
    else
        disp('Build of ReduceAffine succeeded.');
    end

    delete('*.obj');
end

end