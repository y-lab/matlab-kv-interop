# MATLAB-kv-interop

## インストールとアンインストール

MATLABからこのディレクトリを開けば使うことができる。
アンインストールも単にこのディレクトリを削除するだけで良い。

## ドキュメント

http://uec-dn.bitbucket.org/docs/matlab-kv-interop
